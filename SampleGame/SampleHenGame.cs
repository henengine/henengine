﻿using System;
using HenEngine.Core;
using HenEngine.Core.Graphics.Camera;
using HenEngine.Core.Services.Control;
using HenEngine.Core.Services.Logger;
using Microsoft.Extensions.DependencyInjection;
using SampleGame.World.Generation;
using SampleGame.World.Objects;

namespace SampleGame {
	public static class SampleHenGame {
		[STAThread]
		public static void Main() {
			using (var game = BuildGame().Build()) {
				game.Load += (sender, args) => {
					var world = new SampleWorldGenerator(new SampleWorldGeneratorParameters {
						Seed = new Random().Next(),
						MinRoomsCount = 5,
						MaxRoomsCount = 10,
						MinimalRoomWidth = 2,
						MaximalRoomWidth = 5,
						MinimalRoomHeight = 2,
						MaximalRoomHeight = 3
					}).GenerateWorld();
					var player = new SamplePlayer(game.Services.GetService<IController>());
					
					world.SpawnObject(player);
					game.Services.GetService<ICamera>().Look(player);
					game.Services.GetService<ILogger>().Info("Game started");

				};
				
				game.Run();
			}
		}

		private static HenGameBuilder BuildGame() {
			return new HenGameBuilder().UseLogger()
									   .UseCamera()
									   .UseControlManager()
									   .UseMainWindow()
									   .UseResourcesManager();
		}
	}
}