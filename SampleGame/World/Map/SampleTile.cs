using HenEngine.Core.World.Map;

namespace SampleGame.World.Map {
	public class SampleTile : Tile {
		public SampleTile() {
			Shape.Texture = "floor";
		}
	}
}