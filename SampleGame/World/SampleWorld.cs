using HenEngine.Core.World;
using HenEngine.Core.World.Map;

namespace SampleGame.World {
	public class SampleWorld : GameWorld {
		public SampleWorld() : base(new TileMap()) { }
	}
}