using System.Collections.Generic;
using HenEngine.Core.Graphics;
using HenEngine.Core.Graphics.Shapes;
using HenEngine.Core.Services.Control;
using HenEngine.Core.World;
using HenEngine.Core.World.Objects;
using OpenTK.Input;

namespace SampleGame.World.Objects {
	public class SamplePlayer : Entity {
		public SamplePlayer(IController controller) {
			Shape = new Rectangle() {
				Width = 1,
				Height = 1,
				Texture = "player"
			};
			MoveSpeed = 1;
			
			controller.BindKey(new List<Key> { Key.W, Key.Up }, () => Move(Direction.Up, 1));
			controller.BindKey(new List<Key> { Key.A, Key.Left }, () => Move(Direction.Left, 1));
			controller.BindKey(new List<Key> { Key.S, Key.Down }, () => Move(Direction.Down, 1));
			controller.BindKey(new List<Key> { Key.D, Key.Right }, () => Move(Direction.Right, 1));
		}
	}
}