using HenEngine.Core.Graphics.Shapes;
using HenEngine.Core.World.Objects;

namespace SampleGame.World.Objects {
	public class SampleMonster : WorldObject {
		public SampleMonster() {
			Shape = new Rectangle() {
				Width = 1,
				Height = 1,
				Texture = "monster"
			};
		}
	}
}