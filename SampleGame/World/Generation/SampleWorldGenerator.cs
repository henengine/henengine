using HenEngine.Core.World;
using HenEngine.Core.World.WorldGenerator;
using SampleGame.World.Map;

namespace SampleGame.World.Generation {
	public class SampleWorldGenerator : WorldGenerator<SampleWorldGeneratorParameters> {
		public SampleWorldGenerator(SampleWorldGeneratorParameters parameters) : base(parameters) { }

		public override GameWorld GenerateWorld() {
			World = new SampleWorld();
			int roomsCount = Rnd.Next(GeneratorParameters.MinRoomsCount, GeneratorParameters.MaxRoomsCount);

			for (int i = 0; i < roomsCount; i++) {
				int roomCenterX = Rnd.Next(-20, 20);
				int roomCenterY = Rnd.Next(-20, 20);
				int roomWidth = Rnd.Next(GeneratorParameters.MinimalRoomWidth, GeneratorParameters.MaximalRoomWidth);
				int roomHeight = Rnd.Next(GeneratorParameters.MinimalRoomHeight, GeneratorParameters.MaximalRoomHeight);

				for (int xIndex = roomCenterX - roomWidth / 2; xIndex < roomCenterX + roomWidth / 2; xIndex++) {
					for (int yIndex = roomCenterY - roomHeight / 2; yIndex < roomCenterY + roomHeight / 2; yIndex++) {
						World.TileMap[xIndex, yIndex] = new SampleTile {
							IsWalkable = true
						};
					}
				}
			}
			
			//spawn point
			World.TileMap[0, 0] = new SampleTile {
				IsWalkable = true
			};
			
			return World;
		}
	}
}