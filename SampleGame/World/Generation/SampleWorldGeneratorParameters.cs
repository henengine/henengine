using HenEngine.Core.World.WorldGenerator;

namespace SampleGame.World.Generation {
	public class SampleWorldGeneratorParameters : WorldGeneratorParameters {
		//Rooms count
		public int MinRoomsCount { get; set; }
		public int MaxRoomsCount { get; set; }
		
		//Rooms width
		public int MinimalRoomWidth { get; set; }
		public int MaximalRoomWidth { get; set; }
		
		//Rooms height
		public int MinimalRoomHeight { get; set; }
		public int MaximalRoomHeight { get; set; }
	}
}