using System.Numerics;

namespace HenEngine.Core.Utils.Extensions {
	public static class Vector2Extensions {
		/// <summary>
		/// Return a point with offsets
		/// </summary>
		/// <param name="point">Point to apply offset</param>
		/// <param name="xOffset">xOffset</param>
		/// <param name="yOffset">yOffset</param>
		/// <returns>Point with offset</returns>
		public static Vector2 WithOffset(this Vector2 point, float xOffset = 0, float yOffset = 0) {
			return new Vector2(point.X + xOffset, point.Y + yOffset);
		}

		/// <summary>
		/// Modify point coordinates with offsets
		/// </summary>
		/// <param name="point">Point to modify</param>
		/// <param name="xOffset">xOffset</param>
		/// <param name="yOffset">yOffset</param>
		/// <returns>Point with modified coordinates</returns>
		public static Vector2 MoveOffset(this Vector2 point, float xOffset = 0, float yOffset = 0) {
			point.X += xOffset;
			point.Y += yOffset;
			return point;
		}
	}
}