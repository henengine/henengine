using System;
using HenEngine.Core.Graphics.Camera;
using HenEngine.Core.Services.Control;
using Microsoft.Extensions.DependencyInjection;

namespace HenEngine.Core {
	/// <summary>
	/// Main class for game crated on HenEngine
	/// </summary>
	public sealed class HenGame : IDisposable {
		/// <summary>
		/// Services used by game
		/// </summary>
		public IServiceProvider Services { get; }

		/// <inheritdoc cref="OpenTK.GameWindow"/>
		public event EventHandler Load;

		/// <inheritdoc cref="OpenTK.GameWindow"/>
		public event EventHandler UnLoad;

		//Game main window
		private readonly HenGameWindow _mainWindow;

		public HenGame(IServiceCollection serviceCollection) {
			Services = serviceCollection.BuildServiceProvider();
			_mainWindow = Services.GetService<HenGameWindow>();
			_mainWindow.MainCamera = Services.GetService<ICamera>();

			_mainWindow.MainCamera.ZoomLevel = ZoomLevel.X8;
			_mainWindow.Load += (sender, args) => Load?.Invoke(this, args);
			_mainWindow.Unload += (sender, args) => UnLoad?.Invoke(this, args);
			_mainWindow.KeyDown += (sender, args) => Services.GetService<IController>().ProcessKeyboardInput(args);
		}
		
		/// <inheritdoc cref="OpenTK.GameWindow"/>
		public void Run() => Run(0);

		/// <inheritdoc cref="OpenTK.GameWindow"/>
		public void Run(double updateRate) => Run(updateRate, 0);

		/// <inheritdoc cref="OpenTK.GameWindow"/>
		public void Run(double updateRate, double frameRate) => 
			_mainWindow.Run(updateRate, frameRate);

		/// <inheritdoc cref="OpenTK.GameWindow"/>
		public void Exit() {
			_mainWindow.Exit();
			Dispose();
		}

		public void Dispose() {
			_mainWindow?.Dispose();
		}
	}
}