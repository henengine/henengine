using HenEngine.Core.Graphics.Camera;
using HenEngine.Core.Services.Control;
using HenEngine.Core.Services.DiceBag;
using HenEngine.Core.Services.Logger;
using HenEngine.Core.Services.ResourceManager;
using Microsoft.Extensions.DependencyInjection;

namespace HenEngine.Core {
	/// <summary>
	/// Builder to configure all services
	/// </summary>
	public class HenGameBuilder {
		//Service container
		private readonly IServiceCollection _services = new ServiceCollection();

		/// <summary>
		/// Use default HenEngine logger
		/// </summary>
		public HenGameBuilder UseLogger() => UseLogger<Logger>();
		
		/// <summary>
		/// Use custom logger
		/// </summary>
		/// <typeparam name="TLogger">Logger implementation</typeparam>
		public HenGameBuilder UseLogger<TLogger>() where TLogger : class, ILogger {
			_services.AddSingleton<ILogger, TLogger>();
			return this;
		}

		/// <summary>
		/// Use default control manager
		/// </summary>
		public HenGameBuilder UseControlManager() => UseControlManager<Controller>();
		
		/// <summary>
		/// Use custom control manager
		/// </summary>
		/// <typeparam name="TController">Control manager implementation</typeparam>
		public HenGameBuilder UseControlManager<TController>() where TController : class, IController {
			_services.AddSingleton<IController, TController>();
			return this;
		}

		/// <summary>
		/// Use default camera
		/// </summary>
		public HenGameBuilder UseCamera() => UseCamera<BindableCamera>();
		
		/// <summary>
		/// Use custom camera
		/// </summary>
		/// <typeparam name="TCamera">Camera implementation</typeparam>
		public HenGameBuilder UseCamera<TCamera>() where TCamera : class, ICamera {
			_services.AddSingleton<ICamera, TCamera>();
			return this;
		}

		/// <summary>
		/// Use default main window
		/// </summary>
		public HenGameBuilder UseMainWindow() => UseMainWindow<HenGameWindow>();
		
		/// <summary>
		/// Use custom main window
		/// </summary>
		/// <typeparam name="TWindow">Custom main window</typeparam>
		public HenGameBuilder UseMainWindow<TWindow>() where TWindow : HenGameWindow {
			_services.AddSingleton<HenGameWindow, TWindow>();
			return this;
		}

		/// <summary>
		/// Use default resource manager
		/// </summary>
		public HenGameBuilder UseResourcesManager() => UseResourcesManager<ResourceManager>();
		
		/// <summary>
		/// Use custom resource manager
		/// </summary>
		/// <typeparam name="TResources">Resource manager implementation</typeparam>
		public HenGameBuilder UseResourcesManager<TResources>() where TResources : class, IResourceManager {
			_services.AddSingleton<IResourceManager, TResources>();
			return this;
		}
		
		/// <summary>
		/// Use dice bag
		/// </summary>
		/// <typeparam name="TResources">Resource manager implementation</typeparam>
		public HenGameBuilder UseDiceBag() {
			_services.AddSingleton<IDiceBag, DiceBag>();
			return this;
		}

		/// <summary>
		/// Create a game!
		/// </summary>
		public HenGame Build() {
			return new HenGame(_services);
		}
	}
}