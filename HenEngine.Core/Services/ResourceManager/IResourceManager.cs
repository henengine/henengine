using HenEngine.Core.Resources;

namespace HenEngine.Core.Services.ResourceManager {
	/// <summary>
	/// Resource manager
	/// </summary>
	public interface IResourceManager {
		/// <summary>
		/// Get specific resource by key
		/// </summary>
		/// <typeparam name="T">Resource type</typeparam>
		/// <returns>Resource registered under that key</returns>
		T GetResource<T>(string resourceKey) where T : Resource;
	}
}