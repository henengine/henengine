using System.Collections.Generic;
using System.Drawing;
using System.IO;
using HenEngine.Core.Resources;

namespace HenEngine.Core.Services.ResourceManager {
	/// <inheritdoc/>
	public sealed class ResourceManager : IResourceManager {
		private readonly IDictionary<string, Resource> _resources = new Dictionary<string, Resource>();

		public ResourceManager() {
			LoadAllResources();
		}
		
		public T GetResource<T>(string resourceKey) where T : Resource {
			return !string.IsNullOrEmpty(resourceKey) && _resources.ContainsKey(resourceKey) 
				? _resources[resourceKey] as T : null;
		}

		private void LoadAllResources() {
			LoadAllTextures();
		}
		
		private void LoadAllTextures() {
			string texturesDirectory = "Resources/Textures/";

			foreach (string file in Directory.GetFiles(texturesDirectory)) {
				string textureKey = Path.GetFileNameWithoutExtension(file);

				_resources[textureKey] = new Texture(new Bitmap(file));
			}
		}
	}
}