namespace HenEngine.Core.Services.DiceBag {
	/// <summary>
	/// Dice types
	/// </summary>
	public enum Dice {
		/// <summary>
		/// D2
		/// </summary>
		D2 = 2,
		
		/// <summary>
		/// D4
		/// </summary>
		D4 = 4,
		
		/// <summary>
		/// D6
		/// </summary>
		D6 = 6,
		
		/// <summary>
		/// D8
		/// </summary>
		D8 = 8,
		
		/// <summary>
		/// D10
		/// </summary>
		D10 = 10,
		
		/// <summary>
		/// D12
		/// </summary>
		D12 = 12,
		
		/// <summary>
		/// D20
		/// </summary>
		D20 = 20,
		
		/// <summary>
		/// D100
		/// </summary>
		D100 = 100,
	}
}