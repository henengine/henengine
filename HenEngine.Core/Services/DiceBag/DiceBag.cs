using System;

namespace HenEngine.Core.Services.DiceBag {
	/// <inheritdoc/>
	public class DiceBag : IDiceBag {
		private Random _rnd = new Random();
		
		/// <inheritdoc/>
		public int RollDice(Dice dice, int amount = 1) {
			int result = 0;

			for (int roll = 0; roll < amount; roll++) {
				result = _rnd.Next(1, (int) dice + 1);
			}

			return result;
		}
	}
}