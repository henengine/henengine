namespace HenEngine.Core.Services.DiceBag {
	/// <summary>
	/// Simple rnd generator looks like dice throwing
	/// </summary>
	public interface IDiceBag {
		/// <summary>
		/// Rolls a dice
		/// </summary>
		/// <param name="dice"><see cref="Dice"/></param>
		/// <param name="amount">Amount of rolls</param>
		int RollDice(Dice dice, int amount = 1);
	}
}