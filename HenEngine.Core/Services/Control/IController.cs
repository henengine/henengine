using System;
using System.Collections.Generic;
using OpenTK.Input;

namespace HenEngine.Core.Services.Control {
	/// <summary>
	/// Control manager
	/// </summary>
	public interface IController {
		/// <summary>
		/// Bind multiple keys to action
		/// </summary>
		/// <param name="keys">Keys to bind</param>
		/// <param name="action">Action to bind</param>
		void BindKey(IEnumerable<Key> keys, Action action);
		
		/// <summary>
		/// Bind key to action
		/// </summary>
		/// <param name="key">Key to bind</param>
		/// <param name="action">Action to bind</param>
		void BindKey(Key key, Action action);

		/// <summary>
		/// Process player input
		/// </summary>
		/// <param name="keyEventArgs">Args to process</param>
		void ProcessKeyboardInput(KeyboardKeyEventArgs keyEventArgs);
	}
}