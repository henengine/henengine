using System;
using System.Collections.Generic;
using OpenTK.Input;

namespace HenEngine.Core.Services.Control {
	/// <inheritdoc/>
	public class Controller : IController {
		private readonly IDictionary<Key, List<Action>> _keyBindings = new Dictionary<Key, List<Action>>();

		/// <inheritdoc/>
		public void BindKey(IEnumerable<Key> keys, Action action) {
			foreach (var key in keys) {
				BindKey(key, action);
			}
		}
		
		/// <inheritdoc/>
		public void BindKey(Key key, Action action) {
			if (_keyBindings.ContainsKey(key)) {
				_keyBindings[key].Add(action);
			} else {
				_keyBindings.Add(key, new List<Action> {action});
			}
		}

		/// <inheritdoc/>
		public void ProcessKeyboardInput(KeyboardKeyEventArgs eventArgs) {
			if (_keyBindings.ContainsKey(eventArgs.Key)) {
				foreach (var handler in _keyBindings[eventArgs.Key]) {
					handler.Invoke();
				}
			}
		}
	}
}