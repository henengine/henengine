using System;

namespace HenEngine.Core.Services.Logger {
	/// <summary>
	/// Logging service
	/// </summary>
	public interface ILogger {
		/// <summary>
		/// Current logging level
		/// </summary>
		LogLevel LoggingLevel { get; set; }
		
		/// <summary>
		/// Log targets
		/// </summary>
		LogTarget LogTarget { get; set; }

		/// <summary>
		/// Prints text with [ERROR] tag
		/// </summary>
		/// <param name="message">Text to print in log</param>
		void Error(string message);
		
		/// <summary>
		/// Prints text with [INFO] tag
		/// </summary>
		/// <param name="message">Text to print in log</param>
		void Info(string message);
		
		/// <summary>
		/// Prints text with [WARNING] tag
		/// </summary>
		/// <param name="message">Text to print in log</param>
		void Warning(string message);

		/// <summary>
		/// Prints text with [ERROR] tag
		/// </summary>
		/// <param name="e"><see cref="Exception"/> to print in log</param>
		void Error(Exception e);
		
		/// <summary>
		/// Prints text with [DEBUG] tag
		/// </summary>
		/// <param name="message">Text to print in log</param>
		void Debug(string message);
	}
}