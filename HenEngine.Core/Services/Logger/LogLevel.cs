namespace HenEngine.Core.Services.Logger {
	/// <summary>
	/// Log levels
	/// </summary>
	public enum LogLevel {
		/// <summary>
		/// None logging level
		/// </summary>
		None = 0,
		
		/// <summary>
		/// Error logging level
		/// </summary>
		Error = 1,
		
		/// <summary>
		/// Warning logging level
		/// </summary>
		Warning = 2,
		
		/// <summary>
		/// Info logging level
		/// </summary>
		Info = 3,
		
		/// <summary>
		/// Debug logging level
		/// </summary>
		Debug = 4,
	}
}