using System;
using System.IO;
using System.Text;

namespace HenEngine.Core.Services.Logger {
	/// <inheritdoc/>
	public class Logger : ILogger {
		private string _fileName;
		private bool _isInitialized = false;

		/// <inheritdoc/>
		public LogLevel LoggingLevel { get; set; } = LogLevel.Debug;

		/// <inheritdoc/>
		public LogTarget LogTarget { get; set; } = LogTarget.Console | LogTarget.File;

		/// <inheritdoc/>
		public void Error(string message) {
			Log(LogLevel.Error, message);
		}

		/// <inheritdoc/>
		public void Error(Exception e) {
			Log(LogLevel.Error, e.ToString());
		}

		/// <inheritdoc/>
		public void Info(string message) {
			Log(LogLevel.Info, message);
		}

		/// <inheritdoc/>
		public void Warning(string message) {
			Log(LogLevel.Warning, message);
		}

		/// <inheritdoc/>
		public void Debug(string message) {
			Log(LogLevel.Debug, message);
		}

		private void Log(LogLevel level, string message) {
			if (!_isInitialized) {
				Initialize();
			}
			if (LoggingLevel < level) return;

			var messageBuilder = new StringBuilder();

			messageBuilder.AppendFormat("[{0}]", DateTime.Now);
			messageBuilder.AppendFormat("{0}: ", GetLogLevel(level));
			messageBuilder.AppendFormat("{0}", message);

			if (LogTarget.HasFlag(LogTarget.Console)) {
				Console.WriteLine(messageBuilder.ToString());
			}
			if (LogTarget.HasFlag(LogTarget.File)) {
				File.AppendAllText(_fileName, messageBuilder.ToString());
			}
		}

		private string GetLogLevel(LogLevel level) {
			switch (level) {
				case LogLevel.Error: return "[ERROR]";
				case LogLevel.Warning: return "[WARNING]";
				case LogLevel.Info: return "[INFO]";
				case LogLevel.Debug: return "[DEBUG]";
				default: return "";
			}
		}

		private void Initialize() {
			if (LogTarget.HasFlag(LogTarget.File)) {
				var logsDirectory = new DirectoryInfo("Logs/");

				if (logsDirectory.Exists) {
					foreach (var fileSystemInfo in logsDirectory.EnumerateFileSystemInfos()) {
						if (fileSystemInfo.LastWriteTimeUtc < DateTime.Now.AddHours(-1)) {
							fileSystemInfo.Delete();
						}
					}
				} else {
					Directory.CreateDirectory("Logs/");
				}
				
				_fileName = $"Logs/{DateTime.Now}.log";
			}
			
			_isInitialized = true;
		}
	}
}