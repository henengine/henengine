using System;

namespace HenEngine.Core.Services.Logger {
	/// <summary>
	/// Log targets
	/// </summary>
	[Flags]
	public enum LogTarget {
		/// <summary>
		/// Console log target
		/// </summary>
		Console = 1,
		
		/// <summary>
		/// File log targets
		/// </summary>
		File = 2,
	}
}