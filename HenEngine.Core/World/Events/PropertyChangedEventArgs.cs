using System;

namespace HenEngine.Core.World.Events {
	/// <summary>
	/// Event args used when object property value changed
	/// </summary>
	/// <typeparam name="T">Property value type</typeparam>
	public class PropertyChangedEventArgs<T> : EventArgs {
		/// <summary>
		/// Old property value
		/// </summary>
		public T OldValue { get; }
		
		/// <summary>
		/// New property value
		/// </summary>
		public T NewValue { get; }

		/// <summary>
		/// Initializes new instance of <see cref="PropertyChangedEventArgs"/>
		/// </summary>
		/// <param name="oldValue">Old property value</param>
		/// <param name="newValue">New property value</param>
		public PropertyChangedEventArgs(T oldValue, T newValue) {
			OldValue = oldValue;
			NewValue = newValue;
		}
	}
}