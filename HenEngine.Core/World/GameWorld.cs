using System.Collections.Generic;
using HenEngine.Core.World.Map;
using HenEngine.Core.World.Objects;

namespace HenEngine.Core.World {
	/// <summary>
	/// Simple game world
	/// </summary>
	public class GameWorld {
		/// <summary>
		/// World map
		/// </summary>
		public TileMap TileMap { get; }
		
		public List<WorldObject> Objects { get; } = new List<WorldObject>();

		public void SpawnObject(WorldObject worldObject) {
			worldObject.World = this;
			worldObject.PositionChanged += (sender, args) => {
				var newTile = TileMap[args.NewValue];
				if (newTile == null || !newTile.IsWalkable) {
					worldObject.Position = args.OldValue;
				}
			};
			Objects.Add(worldObject);
		}
		
		/// <summary>
		/// Initializes new world
		/// </summary>
		/// <param name="tileMap"></param>
		protected GameWorld(TileMap tileMap) {
			TileMap = tileMap;
		}
	}
}