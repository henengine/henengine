using System;
using HenEngine.Core.Utils.Extensions;
using HenEngine.Core.World.Objects.Abstractions;

namespace HenEngine.Core.World.Objects {
	/// <summary>
	/// Simple entity
	/// </summary>
	public class Entity : WorldObject, IMovable {
		/// <inheritdoc cref="IMovable.Move"/>
		public float MoveSpeed { get; set; }

		/// <inheritdoc cref="IMovable.Move"/>
		public void Move(Direction direction, float distance) {
			switch (direction) {
				case Direction.Up:
					Position = Position.WithOffset(yOffset: distance * MoveSpeed);
					break;
				case Direction.Right:
					Position =  Position.WithOffset(xOffset: distance * MoveSpeed);
					break;
				case Direction.Down:
					Position = Position.WithOffset(yOffset: distance * -MoveSpeed);
					break;
				case Direction.Left:
					Position = Position.WithOffset(xOffset: distance * -MoveSpeed);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
			}
		}
	}
}