namespace HenEngine.Core.World.Objects.Abstractions {
	/// <summary>
	/// Ability to move yourself around the world
	/// </summary>
	public interface IMovable {
		/// <summary>
		/// Moving speed
		/// </summary>
		float MoveSpeed { get; set; }
		
		/// <summary>
		/// Move <see cref="IMovable"/>
		/// </summary>
		/// <param name="direction">Move direction</param>
		/// <param name="distance">Move distance</param>
		void Move(Direction direction, float distance);
	}
}