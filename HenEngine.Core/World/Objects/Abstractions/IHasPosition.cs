using System;
using System.Numerics;
using HenEngine.Core.World.Events;

namespace HenEngine.Core.World.Objects.Abstractions {
	/// <summary>
	/// Ability to stay in the world
	/// </summary>
	public interface IHasPosition {
		/// <summary>
		/// World position
		/// </summary>
		Vector2 Position { get; set; }

		/// <summary>
		/// Raises when Position changed
		/// </summary>
		event EventHandler<PropertyChangedEventArgs<Vector2>> PositionChanged;
	}
}