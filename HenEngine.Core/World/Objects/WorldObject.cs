using System;
using System.Numerics;
using System.Drawing;
using HenEngine.Core.Graphics;
using HenEngine.Core.Graphics.Shapes;
using HenEngine.Core.World.Events;
using HenEngine.Core.World.Objects.Abstractions;

namespace HenEngine.Core.World.Objects {
	public class WorldObject : IRenderable {
		private Vector2 _position;
		
		/// <inheritdoc cref="IRenderable.Shape"/>
		public Shape Shape { get; set; }

		/// <inheritdoc cref="IRenderable.IsVisible"/>
		public bool IsVisible { get; set; }
		
		/// <summary>
		/// Object position in the world
		/// </summary>
		public Vector2 Position {
			get => _position;
			set {
				var oldValue = _position;
				_position = value;
				OnPositionChanged(oldValue, _position);
			}
		}

		/// <summary>
		/// World with this object
		/// </summary>
		public GameWorld World { get; set; }
		
		/// <inheritdoc cref="IHasPosition.PositionChanged"/>
		public event EventHandler<PropertyChangedEventArgs<Vector2>> PositionChanged;

		protected void OnPositionChanged(Vector2 oldPosition, Vector2 newPosition) {
			var eventArgs = new PropertyChangedEventArgs<Vector2>(oldPosition, newPosition);
			PositionChanged?.Invoke(this, eventArgs);
		}
	}
}