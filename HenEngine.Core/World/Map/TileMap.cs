using System.Numerics;
using HenEngine.Core.Data;

namespace HenEngine.Core.World.Map {
	/// <summary>
	/// Basic class for world map
	/// </summary>
	public class TileMap {
		private readonly Negative2DCollection<Tile> _map = new Negative2DCollection<Tile>();

		public Tile this[Vector2 coordinates] {
			get => _map[(int) coordinates.X, (int) coordinates.Y];
			set => _map[(int) coordinates.X, (int) coordinates.Y] = value;
		}

		public Tile this[int xPos, int yPos] {
			get => _map[xPos, yPos];
			set => _map[xPos, yPos] = value;
		}
	}
}