using System.Drawing;
using HenEngine.Core.Graphics;
using HenEngine.Core.Graphics.Shapes;
using Rectangle = HenEngine.Core.Graphics.Shapes.Rectangle;

namespace HenEngine.Core.World.Map {
	/// <summary>
	/// Base tile
	/// </summary>
	public class Tile : IRenderable {
		/// <summary>
		/// Is that tile walkable
		/// </summary>
		public bool IsWalkable { get; set; } = false;
		
		/// <inheritdoc/>
		public Shape Shape { get; set; } = new Rectangle() {
			Height = 1,
			Width = 1,
			Texture = "",
			FillColor = Color.DarkGray
		};

		/// <inheritdoc/>
		public bool IsVisible { get; set; }
	}
}