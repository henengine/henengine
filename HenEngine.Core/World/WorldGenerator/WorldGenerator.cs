using System;

namespace HenEngine.Core.World.WorldGenerator {
	/// <summary>
	/// Abstract world generator
	/// </summary>
	/// <typeparam name="TP">World generation parameters</typeparam>
	public abstract class WorldGenerator<TP> where TP : WorldGeneratorParameters {
		protected GameWorld World;
		protected readonly Random Rnd;
		protected readonly TP GeneratorParameters;

		/// <summary>
		/// Initializes new instance of <see cref="WorldGemerator"/>
		/// </summary>
		/// <param name="generatorParameters">World generator parameters</param>
		protected WorldGenerator(TP generatorParameters) {
			GeneratorParameters = generatorParameters;
			Rnd = new Random(generatorParameters.Seed);
		}
		
		/// <summary>
		/// Starts world generation
		/// </summary>
		public abstract GameWorld GenerateWorld();
	}
}