namespace HenEngine.Core.World.WorldGenerator {
	/// <summary>
	/// Parameters used by <see cref="WorldGenerator"/> to generate <see cref="GameWorld"/>
	/// </summary>
	public abstract class WorldGeneratorParameters {
		/// <summary>
		/// Start seed for world generator
		/// </summary>
		public int Seed { get; set; }
	}
}