using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HenEngine.Core.Data {
	/// <summary>
	/// Collection that supports negative indexes
	/// </summary>
	/// <typeparam name="T">Item type</typeparam>
	public class Negative1DCollection<T> : IEnumerable<T> {
		private readonly List<T> _positive = new List<T>();
		private readonly List<T> _negative = new List<T>();

		public T this[int index] {
			get => List(index).ElementAtOrDefault(Math.Abs(index));
			set => Add(value, index);
		}

		public IEnumerator<T> GetEnumerator() {
			for (int i = _negative.Count - 1; i < _positive.Count; i++) {
				yield return this[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}

		private void Add(T item, int index) {
			var list = List(index);
			if (list.Count <= Math.Abs(index)) {
				int missingCount = Math.Abs(index) - (list.Count - 1);
				list.AddRange(Enumerable.Repeat(default(T), missingCount));
			}
			list[Math.Abs(index)] = item;
		}

		private List<T> List(int index) => index < 0 ? _negative : _positive;
	}
}