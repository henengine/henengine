using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;

namespace HenEngine.Core.Data {
	/// <summary>
	/// Two-dimensional collection that supports negative indexes
	/// </summary>
	/// <typeparam name="T">Items type</typeparam>
	public class Negative2DCollection<T> : IEnumerable<T> {
		private readonly Negative1DCollection<Negative1DCollection<T>> _values = new Negative1DCollection<Negative1DCollection<T>>();
		
		public T this[int xPosition, int yPosition] {
			get {
				var row = _values[xPosition];
				return row != null ? row[yPosition] : default(T);
			}
			set {
				var row = _values[xPosition] ?? (_values[xPosition] = new Negative1DCollection<T>());
				row[yPosition] = value;
			}
		}

		public IEnumerator<T> GetEnumerator() {
			return _values.Where(row => row != null).SelectMany(row => row).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
	}
}