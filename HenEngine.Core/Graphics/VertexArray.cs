using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using OpenTK.Graphics.OpenGL;

namespace HenEngine.Core.Graphics {
	/// <summary>
	/// Simple vertex array. Used as part of <see cref="IRenderable"/>
	/// </summary>
	public class VertexArray : IEnumerable<Vertex> {
		private readonly List<Vertex> _vertices;
		
		/// <summary>
		/// <see cref="PrimitiveType"/> to render that <see cref="VertexArray"/>
		/// </summary>
		public PrimitiveType PrimitiveType { get; }

		/// <summary>
		/// Get <see cref="Vertex"/> at specific index
		/// </summary>
		/// <param name="index"></param>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public Vertex this[int index] {
			get {
				if (index >= _vertices.Count || index < 0) {
					throw new ArgumentOutOfRangeException();
				}

				return _vertices[index];
			}
			set {
				if (index >= _vertices.Count || index < 0) {
					throw new ArgumentOutOfRangeException();
				}

				_vertices[index] = value;
			}
		}

		/// <summary>
		/// <see cref="VertexArray"/> vertices count
		/// </summary>
		public int Count => _vertices.Count;

		/// <summary>
		/// Initializes new instance of <see cref="VertexArray"/>
		/// </summary>
		/// <param name="primitiveType"><see cref="PrimitiveType"/> to render that <see cref="VertexArray"/></param>
		/// <param name="count">Vertices count</param>
		public VertexArray(PrimitiveType primitiveType, int count = 0) 
			: this(primitiveType, Enumerable.Repeat(new Vertex(new Vector2(0f, 0f), 
															   new Vector2(0f, 0f)), count)){ }

		/// <summary>
		/// Initializes new instance of <see cref="VertexArray"/>
		/// </summary>
		/// <param name="primitiveType"><see cref="PrimitiveType"/> to render that <see cref="VertexArray"/></param>
		/// <param name="vertices">Vertices contained in that <see cref="VertexArray"/></param>
		public VertexArray(PrimitiveType primitiveType, IEnumerable<Vertex> vertices) {
			PrimitiveType = primitiveType;
			_vertices = vertices.ToList();
		}
		
		/// <summary>
		/// Get bounds of that <see cref="VertexArray"/>
		/// </summary>
		/// <returns></returns>
		public RectangleF GetBounds() {
			if (_vertices == null || _vertices.Count == 0) {
				return RectangleF.Empty;
			}

			float left = _vertices[0].Position.X;
			float right = _vertices[0].Position.X;
			float top = _vertices[0].Position.Y;
			float bottom = _vertices[0].Position.Y;
			
			foreach (var vertex in _vertices) {
				if (vertex.Position.X < left) {
					left = vertex.Position.X;
				} else if (vertex.Position.X > right) {
					right = vertex.Position.X;
				}

				if (vertex.Position.Y < top) {
					top = vertex.Position.Y;
				} else if (vertex.Position.Y > bottom) {
					bottom = vertex.Position.Y;
				}
			}
			
			return new RectangleF(left, top, right - left, bottom - top);
		}
		
		public Vertex[] ToArray() {
			return _vertices.ToArray();
		}

		public IEnumerator<Vertex> GetEnumerator() => _vertices.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}

		public static implicit operator Vertex[](VertexArray vertices) {
			return vertices.ToArray();
		}
	}
}