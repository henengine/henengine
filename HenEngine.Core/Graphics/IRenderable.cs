using HenEngine.Core.Graphics.Shapes;

namespace HenEngine.Core.Graphics {
	/// <summary>
	/// Interface used by objects to render
	/// </summary>
	public interface IRenderable {
		/// <summary>
		/// Textured box for that object
		/// </summary>
		Shape Shape { get; set; }
		
		/// <summary>
		/// Is that object visible
		/// </summary>
		bool IsVisible { get; set; }
	}
}