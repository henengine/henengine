using System.Drawing;
using System.Numerics;

namespace HenEngine.Core.Graphics.Shapes {
	/// <summary>
	/// Simple rectangle
	/// </summary>
	/// <inheritdoc/>
	public class Rectangle : Shape {
		private SizeF _size;

		/// <summary>
		/// <see cref="Rectangle"/> size
		/// </summary>
		public SizeF Size {
			get => _size;
			set {
				_size = value;
				Update();
			}
		}

		/// <summary>
		/// <see cref="Rectangle"/> width
		/// </summary>
		public float Width {
			get => _size.Width;
			set => Size = new SizeF(value, Size.Height);
		}
		
		/// <summary>
		/// <see cref="Rectangle"/> height
		/// </summary>
		public float Height {
			get => _size.Height;
			set => Size = new SizeF(Size.Width, value);
		}

		/// <summary>
		/// Initializes new instance of <see cref="Rectangle"/>
		/// </summary>
		public Rectangle() : this(SizeF.Empty) { }

		/// <summary>
		/// Initializes new instance of <see cref="Rectangle"/>
		/// </summary>
		/// <param name="size">Rectangle size</param>
		public Rectangle(SizeF size)  {
			Size = size;
		}

		protected override int GetPointCount() {
			return 4;
		}

		protected override Vector2 GetPoint(int index) {
			switch (index) {
				default:
				case 0: return new Vector2(0f, 0f);
				case 1: return new Vector2(_size.Width, 0f);
				case 2: return new Vector2(_size.Width, _size.Height);
				case 3: return new Vector2(0f, _size.Height);
			}
		}
	}
}