using System.Drawing;
using System.Numerics;
using OpenTK.Graphics.OpenGL;

namespace HenEngine.Core.Graphics.Shapes {
	/// <summary>
	/// Simple shape used as part of <see cref="IRenderable"/>
	/// </summary>
	public abstract class Shape {
		/// <summary>
		/// Shape texture
		/// </summary>
		public string Texture { get; set; }
		
		/// <summary>
		/// Shape fill color
		/// </summary>
		public Color FillColor { get; set; }
		
		/// <summary>
		/// Array of that shape vertices
		/// </summary>
		public VertexArray Vertices { get; protected set; }
		
		/// <summary>
		/// Shape bounds
		/// </summary>
		public RectangleF Bounds { get; protected set; }

		/// <summary>
		/// Initializes new instance of <see cref="Shape"/>
		/// </summary>
		public Shape() : this(PrimitiveType.TriangleFan) { }

		/// <summary>
		/// Initializes new instance of <see cref="Shape"/>
		/// </summary>
		/// <param name="primitiveType"><see cref="PrimitiveType"/> used to render this <see cref="Shape"/></param>
		public Shape(PrimitiveType primitiveType) {
			Texture = null;
			FillColor = Color.White;
			Vertices = new VertexArray(primitiveType);
			Bounds = RectangleF.Empty;
		}

		/// <summary>
		/// Update all points of that <see cref="Shape"/>
		/// </summary>
		protected void Update() {
			int pointCount = GetPointCount();

			if (pointCount < 3) {
				Vertices = new VertexArray(Vertices.PrimitiveType);
				return;
			}
			
			Vertices = new VertexArray(Vertices.PrimitiveType, pointCount + 2);
			
			for (int i = 0; i < pointCount; i++) {
				var vertex = Vertices[i + 1];
				vertex.Position = GetPoint(i);
				Vertices[i + 1] = vertex;
			}

			Vertices[pointCount + 1] = new Vertex(Vertices[1].Position, Vertices[pointCount + 1].TexturePosition, Vertices[pointCount + 1].Color);
			Vertices[0] = Vertices[1];
			Bounds = Vertices.GetBounds();

			var center = Vertices[0];
			
			center.Position = new Vector2(Bounds.Left + Bounds.Width / 2, Bounds.Top + Bounds.Height / 2);
			Vertices[0] = center;
			
			UpdateTexture();
		}
		
		protected abstract int GetPointCount();
		protected abstract Vector2 GetPoint(int index);
		
		private void UpdateTexture() {
			for (int i = 0; i < Vertices.Count; i++) {
				var vertex = Vertices[i];
				vertex.TexturePosition = new Vector2(vertex.Position.X, vertex.Position.Y);
				Vertices[i] = vertex;
			}
		}
	}
}