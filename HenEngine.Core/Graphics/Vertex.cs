using System.Drawing;
using System.Numerics;
using System.Runtime.InteropServices;

namespace HenEngine.Core.Graphics {
	/// <summary>
	/// Simple vertex
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct Vertex {
		/// <summary>
		/// <see cref="Vertex"/> position
		/// </summary>
		public Vector2 Position { get; set; }
		
		/// <summary>
		/// <see cref="Vertex"/> texture position
		/// </summary>
		public Vector2 TexturePosition { get; set; }
		
		/// <summary>
		/// <see cref="Vertex"/> color
		/// </summary>
		public Color Color { get; set; }

		/// <summary>
		/// Initializes new instance of <see cref="Vertex"/>
		/// </summary>
		/// <param name="value">Position value</param>
		public Vertex(float value) : this(value, Color.White) {}
		
		/// <summary>
		/// Initializes new instance of <see cref="Vertex"/>
		/// </summary>
		/// <param name="position"><see cref="Vertex"/> position</param>
		/// <param name="texturePosition"><see cref="Vertex"/> texture position</param>
		public Vertex(Vector2 position, Vector2 texturePosition) : this(position, texturePosition, Color.White) { }

		/// <summary>
		/// Initializes new instance of <see cref="Vertex"/>
		/// </summary>
		/// <param name="vertex">Vertex to copy</param>
		public Vertex(Vertex vertex) {
			Position = vertex.Position;
			TexturePosition = vertex.TexturePosition;
			Color = vertex.Color;
		}
		
		/// <summary>
		/// Initializes new instance of <see cref="Vertex"/>
		/// </summary>
		/// <param name="value">Position value</param>
		/// <param name="color"><see cref="Vertex"/> color</param>
		public Vertex(float value, Color color) {
			Position = new Vector2(value);
			TexturePosition = new Vector2(value);
			Color = color;
		}

		/// <summary>
		/// Initializes new instance of <see cref="Vertex"/>
		/// </summary>
		/// <param name="position"><see cref="Vertex"/> position</param>
		/// <param name="texturePosition"><see cref="Vertex"/> texture position</param>
		/// <param name="color"><see cref="Vertex"/> color</param>
		public Vertex(Vector2 position, Vector2 texturePosition, Color color) {
			Position = position;
			TexturePosition = texturePosition;
			Color = color;
		}

		public static bool operator ==(Vertex v1, Vertex v2) => v1.Equals(v2);
		
		public static bool operator !=(Vertex v1, Vertex v2) => !v1.Equals(v2);

		public override bool Equals(object obj) {
			Vertex right = (Vertex) obj;
			return Position == right.Position 
				&& TexturePosition == right.TexturePosition
				&& Color == right.Color;
		}

		public override int GetHashCode() {
			return Position.GetHashCode() ^ TexturePosition.GetHashCode() ^ Color.GetHashCode();
		}
	}
}