using System.Drawing;
using System.Numerics;
using HenEngine.Core.Graphics.Renderers;
using HenEngine.Core.Services.ResourceManager;
using HenEngine.Core.Utils.Extensions;
using HenEngine.Core.World;
using HenEngine.Core.World.Map;
using HenEngine.Core.World.Objects;

namespace HenEngine.Core.Graphics.Camera {
	/// <inheritdoc cref="ICamera"/>
	public class BindableCamera : ICamera {
		/// <inheritdoc cref="ICamera"/>
		public ZoomLevel ZoomLevel { get; set; } = ZoomLevel.X1;

		/// <summary>
		/// Camera position in the world
		/// </summary>
		public Vector2 Position => _worldObject.Position;
		
		/// <summary>
		/// Camera world
		/// </summary>
		public GameWorld World { get; private set; }

		private WorldObject _worldObject;
		private readonly IRenderer _renderer;

		public BindableCamera(IResourceManager resourceManager) {
			_renderer = new Renderer(resourceManager);
		}

		/// <inheritdoc cref="ICamera"/>
		public void Render() {
			float tileSize = 2f / (int) ZoomLevel;
			int tilesToDraw = (int) ZoomLevel / 2;
			
			//Draw tiles
			for (int xOffset = -tilesToDraw * 2; xOffset < tilesToDraw * 2; xOffset++) {
				for (int yOffset = -tilesToDraw * 2; yOffset < tilesToDraw * 2; yOffset++) {
					var tile = World.TileMap[_worldObject.Position.WithOffset(xOffset, yOffset)] ?? new Tile();
					var tileBox = CalculateRenderPosition(tile, _worldObject.Position.WithOffset(xOffset, yOffset), tileSize);

					_renderer.Render(tile, tileBox);
				}
			}

			//Draw world objects
			foreach (var worldObject in World.Objects) {
				if (worldObject != _worldObject
				 && worldObject.Position.X >= Position.X - tilesToDraw &&
					worldObject.Position.X <= Position.X + tilesToDraw
				 && worldObject.Position.Y >= Position.Y - tilesToDraw &&
					worldObject.Position.Y <= Position.Y + tilesToDraw) {
					var objectBox = CalculateRenderPosition(worldObject, worldObject.Position, tileSize);
					
					_renderer.Render(worldObject, objectBox);
				}
			}
			
			//Draw actor
			var actorBox = CalculateRenderPosition(_worldObject, _worldObject.Position, tileSize);
			
			_renderer.Render(_worldObject, actorBox);
		}

		/// <inheritdoc cref="ICamera"/>
		public void Look(WorldObject target) {
			_worldObject = target;
			World = _worldObject.World;
		}

		/// <summary>
		/// Calculate in-screen box to render
		/// </summary>
		/// <param name="renderable">IRenderable to render</param>
		/// <param name="renderablePosition">Renderable position</param>
		/// <param name="tileSize">One world cell size</param>
		/// <returns>Box with render coordinates</returns>
		private RectangleF CalculateRenderPosition(IRenderable renderable, Vector2 renderablePosition, float tileSize) {
			float xOffset = (renderablePosition.X - Position.X) * tileSize;
			float yOffset = (renderablePosition.Y - Position.Y) * tileSize;

			return new RectangleF(xOffset - renderable.Shape.Bounds.Width * tileSize / 2,
								  yOffset + renderable.Shape.Bounds.Height * tileSize / 2,
								  renderable.Shape.Bounds.Width * tileSize,
								  -renderable.Shape.Bounds.Height * tileSize);
		}
	}
}