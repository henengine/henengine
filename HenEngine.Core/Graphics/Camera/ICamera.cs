using HenEngine.Core.World.Objects;

namespace HenEngine.Core.Graphics.Camera {
	/// <summary>
	/// Camera that renders a world
	/// </summary>
	public interface ICamera {
		/// <summary>
		/// Camera zoom level
		/// </summary>
		ZoomLevel ZoomLevel { get; set; }
		
		/// <summary>
		/// Renders world part visible through this camera
		/// </summary>
		void Render();

		/// <summary>
		/// Centers a camera on a target
		/// </summary>
		/// <param name="target">Target to center camera</param>
		void Look(WorldObject target);
	}
}