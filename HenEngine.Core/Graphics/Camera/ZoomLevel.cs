namespace HenEngine.Core.Graphics.Camera {
	/// <summary>
	/// Camera zoom levels to define a 
	/// </summary>
	public enum ZoomLevel {
		X1 = 1,
		X2 = 2,
		X4 = 4,
		X8 = 8,
		X16 = 16
	}
}