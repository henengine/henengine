using System.Drawing;

namespace HenEngine.Core.Graphics.Renderers {
	/// <summary>
	/// Renderer interface
	/// </summary>
	public interface IRenderer {
		/// <summary>
		/// Render <see cref="IRenderable"/> in screen rectangle
		/// </summary>
		/// <param name="renderable"><see cref="IRenderable"/> to render</param>
		/// <param name="screenBox">Screen rectangle to render <see cref="IRenderable"/></param>
		void Render(IRenderable renderable, RectangleF screenBox);
	}
}