using System.Drawing;
using HenEngine.Core.Resources;
using HenEngine.Core.Services.ResourceManager;
using OpenTK.Graphics.OpenGL;

namespace HenEngine.Core.Graphics.Renderers {
	/// <inheritdoc/>
	/// <summary>
	/// Renderer that uses legacy OpenGL render methods
	/// </summary>
	public class Renderer : IRenderer {
		private readonly IResourceManager _resourceManager;

		/// <summary>
		/// Initializes new instance of <see cref="Renderer"/>
		/// </summary>
		/// <param name="resourceManager">Resource manager</param>
		public Renderer(IResourceManager resourceManager) {
			_resourceManager = resourceManager;
		}
		
		/// <inheritdoc/>
		public void Render(IRenderable renderable, RectangleF screenBox) {
			string textureKey = string.IsNullOrEmpty(renderable.Shape.Texture) 
				? "stone" : renderable.Shape.Texture;
			
			_resourceManager.GetResource<Texture>(textureKey)?.Bind();
			GL.Begin(renderable.Shape.Vertices.PrimitiveType);

			foreach (var vertex in renderable.Shape.Vertices) {
				float xPos = screenBox.X + vertex.Position.X * screenBox.Width;
				float yPos = screenBox.Y + vertex.Position.Y * screenBox.Height;
				
				GL.Color3(renderable.Shape.FillColor);
				GL.TexCoord2(vertex.TexturePosition.X, vertex.TexturePosition.Y);
				GL.Vertex2(xPos, yPos);
			}
			
			GL.End();
			GL.BindTexture(TextureTarget.Texture2D, 0);
		}
	}
}