using System;
using HenEngine.Core.Graphics.Camera;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace HenEngine.Core {
	/// <inheritdoc />
	public class HenGameWindow : GameWindow {
		
		public HenGameWindow() : base(800, 800, GraphicsMode.Default, "HenEngine") {}
		
		public ICamera MainCamera { get; set; }

		protected override void OnLoad(EventArgs e) {
			base.OnLoad(e);
			
			GL.Enable(EnableCap.Texture2D);
			GL.Enable(EnableCap.Blend);
			
			GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
		}

		protected override void OnResize(EventArgs e) {
			base.OnResize(e);

			GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);
		}

		protected override void OnRenderFrame(FrameEventArgs e) {
			base.OnRenderFrame(e);
			
			GL.ClearColor(Color4.White);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			MainCamera?.Render();
			
			SwapBuffers();
		}
	}
}