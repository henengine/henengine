using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using OpenTK.Graphics.OpenGL;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace HenEngine.Core.Resources {
	/// <summary>
	/// Game texture
	/// </summary>
	public class Texture : Resource {
		/// <summary>
		/// Texture width
		/// </summary>
		public int Width { get; private set; }
		
		/// <summary>
		/// Texture height
		/// </summary>
		public int Height { get; private set; }

		public Texture(byte[] data) {
			using (var ms = new MemoryStream(data)) {
				var bitmap = new Bitmap(ms);
				
				ProcessBitmap(bitmap);
			}
		}
		
		public Texture(Bitmap bitmap) => ProcessBitmap(bitmap);

		public void Bind() {
			GL.BindTexture(TextureTarget.Texture2D, Handle);
		}

		private void ProcessBitmap(Bitmap bitmap) {
			Handle = GL.GenTexture();
			Bind();

			Width = bitmap.Width;
			Height = bitmap.Height;

			var bitmapData = bitmap.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.ReadOnly,
											 PixelFormat.Format32bppArgb);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmapData.Width, bitmapData.Height, 0,
						  OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0);
			bitmap.UnlockBits(bitmapData);

			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter,
							(int) TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter,
							(int) TextureMagFilter.Linear);
		}
	}
}