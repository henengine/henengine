namespace HenEngine.Core.Resources {
	/// <summary>
	/// Base resource
	/// </summary>
	public abstract class Resource {
		/// <summary>
		/// Resource handle
		/// </summary>
		public int Handle { get; protected set; }
	}
}